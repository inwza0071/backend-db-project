import { IsNotEmpty, MinLength } from 'class-validator';
export class CreateProductDto {
  id: number;

  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  price: number;
}
